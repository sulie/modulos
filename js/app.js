// importación de elementos de api.js e interfaz.js

// manejador de eventos para el envío del formulario

// Se deberá controlar si los campos están vacíos ya que son obligatorios para la consulta a la API. 

// Campos vacíos: mostrar error en contenedor "mensajes" añadiéndole el estilo de clase ".error" durante 3 segundos.

// Campos cubiertos, consulta a la API a través de la clase API del archivo api.js y gestión de la respuesta. En caso de no obtener resultados tras la consulta, mostrar error de nuevo durante 3segundos en el mismo contenedor, y se reseteará el formulario para que se pueda realizar una nueva búsqueda.

// Tener en cuenta que si la consulta de una canción obtiene respuesta, la letra se encuentra en la propiedad respuesta.lyrics

import {artista,cancion,mensajes,resultado,formulario} from './interfaz.js';
import {songAPI} from './api.js';

<<<<<<< HEAD
formulario.addEventListener('submit', async () => {
    if (artista == undefined || cancion == undefined) {
        mensajes.classList.add('error');
        mensajes.innerHTML = '<p>Los campos son obligatorios</p>';

        await setTimeout( () => { 
            mensajes.innerHTML = '';
            mensajes.classList.remove('error');
        }, 3000);

    } else if (artista != undefined && cancion != undefined) {
        let song = await new songAPI(artista.value,cancion.value);
        let lyrics = await song.getLyrics();
        if (lyrics.lyrics != undefined) {
            resultado.innerHTML = lyrics.lyrics;
            mensajes.innerHTML = '';
            mensajes.classList.remove('error');
        } else {
            mensajes.classList.add('error');
            mensajes.innerHTML = 'Por favor introduzca un artista y una canción válidos';
        }
    }
=======
formulario.addEventListener('submit', async function(){
    var peticion = new songAPI(artista.value,cancion.value);
    var respuesta = await peticion.getLyrics();
    resultado.innerHTML = respuesta;
>>>>>>> 077ae4c04d91347ef663affe82c9a601599c37df
});