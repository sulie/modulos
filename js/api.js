// exportar la clase API que se crear� con dos m�todos:
// constructor con dos par�metros: artista y canci�n
// funci�n async para realizar la consulta utilizando fetch. La respuesta se devolver� en formato json. El resultado se mostrar� (recuperar�) en el archivo app.js
// Direcci�n API ayuda : https://lyricsovh.docs.apiary.io/#reference/0/lyrics-of-a-song/search
// Formato para la consulta: https://api.lyrics.ovh/v1/artist/title
// sustituyendo artist por el artista a consultar y title por la canci�n correspondiente

export class songAPI {
    constructor (artista, cancion) {
        this.artista = artista;
        this.cancion = cancion;
    }

    async getLyrics() {
        try {
            const url = 'https://api.lyrics.ovh/v1/';
            let response = await fetch(url+this.artista+'/'+this.cancion);
            let data = await response.json();
            return data;
        }
        catch (error) {
            return error;
        }
    }
}